import {calResult} from './TellMeResult';

test('給 calResult 無效的參數，會傳回 invalid', () => {
  expect(calResult('', '')).toEqual('invalid');
  expect(calResult('2256', '234')).toEqual('invalid');
  expect(calResult('234', '6677')).toEqual('invalid');
});

test('calResult 能夠判斷有幾個 A', () => {
  expect(calResult('1509', '8507')).toEqual('2A0B');
  expect(calResult('0638', '0638')).toEqual('4A0B');
});

test('calResult 能正確判斷幾A幾B', () => {
  expect(calResult('4569', '3456')).toEqual('0A3B');
  expect(calResult('2367', '1267')).toEqual('2A1B');
});
